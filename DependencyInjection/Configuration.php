<?php

namespace ATM\MotwBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_motw')
                ->children()
                    ->scalarNode('user')->isRequired()->end()
                    ->scalarNode('media_folder')->isRequired()->end()
                    ->scalarNode('redirect_route_motw_not_exists')->isRequired()->end()
                ->end();

        return $treeBuilder;
    }
}
