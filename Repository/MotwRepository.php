<?php

namespace ATM\MotwBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MotwRepository extends EntityRepository{

    public function getForbiddenDates()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select('m.end_date')
            ->from('ATMMotwBundle:Motw','m')
            ->orderBy('m.end_date','DESC');


        $results = $qb->getQuery()->getArrayResult();

        return isset($results[0]) ? $results[0]['end_date'] : null;
    }
}