<?php

namespace ATM\MotwBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use ATM\MotwBundle\Entity\Motw;

class MotwType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('model',EntityType::class,array(
                'class' => $options['user'],
                'query_builder' => function($er) {
                    $qb = $er->createQueryBuilder('u');
                    return $qb
                        ->where(
                            $qb->expr()->andX(
                                $qb->expr()->like('u.roles',$qb->expr()->literal('%MODEL%')),
                                $qb->expr()->eq('u.enabled',1),
                                $qb->expr()->eq('u.approved',1),
                                $qb->expr()->eq('u.locked',0)
                            )
                        )
                        ->orderBy('u.username', 'ASC');
                },
                'choice_label' => 'username',
                'placeholder' => 'Chose a model',
                'empty_data'  => null,
                'required' => true
            ))
            ->add('description',TextareaType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Description',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'End Date',
                    'autocomplete' => 'off'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'data' => Motw::class
        ));
    }
}