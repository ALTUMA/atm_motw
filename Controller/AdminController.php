<?php

namespace ATM\MotwBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ATM\MotwBundle\Entity\Motw;
use ATM\MotwBundle\Form\MotwType;
use ATM\MotwBundle\Entity\Spyder\Task;
use ATM\MotwBundle\Entity\Spyder\Encoding;
use ATM\MotwBundle\Services\ImageManager;

class AdminController extends Controller{

    /**
     * @Route("/index/{page}", name="atm_motw_index", defaults={"page":1})
     */
    public function indexAction($page){
        $em = $this->getDoctrine()->getManager();

        $qbIds = $em->createQueryBuilder();
        $qbIds
            ->select('m.id')
            ->from('ATMMotwBundle:Motw','m')
            ->orderBy('m.init_date','DESC');

        $arrIds = array_map(function($r){
            return $r['id'];
        },$qbIds->getQuery()->getArrayResult());

        $motws = null;
        $pagination = null;
        if(count($arrIds) > 0){
            $pagination = $this->get('knp_paginator')->paginate(
                $arrIds,
                $page,
                15
            );

            $ids = $pagination->getItems();

            $qb = $em->createQueryBuilder();
            $qb
                ->select('m')
                ->addSelect('model')
                ->from('ATMMotwBundle:Motw','m')
                ->join('m.model','model')
                ->where($qb->expr()->in('m.id',$ids))
                ->orderBy('m.init_date','DESC');

            $motws = $qb->getQuery()->getArrayResult();
        }

        return $this->render('ATMMotwBundle:Admin:index.html.twig',array(
            'motws' => $motws,
            'pagination' => $pagination
        ));
    }


    /**
     * @Route("/create", name="atm_motw_create")
     */
    public function createAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_motw_config');

        $motw = new Motw();
        $form = $this->createForm(MotwType::class,$motw,array(
            'user' => $config['user']
        ));

        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/';
                $this->createFolder($mediaDir);

                $folderName = $request->get('folderName');
                $motw->setFolderName($folderName);
                $motwFolder = $mediaDir.$folderName;
                $this->createFolder($motwFolder);

                $thumbnailFileTour = $request->files->get('motwThumbnailTour');
                $thumbNameTour = md5(uniqid()).'.'. $thumbnailFileTour->guessExtension();
                $thumbnailFileTour->move($motwFolder,$thumbNameTour);
                $motw->setThumbnailTour($config['media_folder'].'/'.$folderName.'/'.$thumbNameTour);

                $thumbnailFileTourMobile = $request->files->get('motwThumbnailTourMobile');
                $thumbNameTourMobile = md5(uniqid()).'.'. $thumbnailFileTourMobile->guessExtension();
                $thumbnailFileTourMobile->move($motwFolder,$thumbNameTourMobile);
                $motw->setThumbnailTourMobile($config['media_folder'].'/'.$folderName.'/'.$thumbNameTourMobile);


                $thumbnailFileMembers = $request->files->get('motwThumbnailMembers');
                $extensionMembers = $thumbnailFileMembers->guessExtension();
                $thumbNameMembers = md5(uniqid()).'.'.$extensionMembers;
                $thumbnailFileMembers->move($motwFolder,$thumbNameMembers);
                $motw->setThumbnailMembers($config['media_folder'].'/'.$folderName.'/'.$thumbNameMembers);

                $thumbnailFileMembersMobile = $request->files->get('motwThumbnailMembersMobile');
                $thumbNameMembersMobile = md5(uniqid()).'.'.$thumbnailFileMembersMobile->guessExtension();
                $thumbnailFileMembersMobile->move($motwFolder,$thumbNameMembersMobile);
                $motw->setThumbnailMembersMobile($config['media_folder'].'/'.$folderName.'/'.$thumbNameMembersMobile);

                $em->persist($motw);
                $em->flush();

                return $this->redirect($this->get('router')->generate('atm_motw_index'));
            }
        }

        $folderName = md5(uniqid());

        $em = $this->getDoctrine()->getManager();
        $forbiddenDate = $em->getRepository('ATMMotwBundle:Motw')->getForbiddenDates();
        $forbiddenDate = !is_null($forbiddenDate) ? $forbiddenDate->modify('+1 day') : null;

        return $this->render('ATMMotwBundle:Admin:create.html.twig',array(
            'form' => $form->createView(),
            'folderName' => $folderName,
            'forbiddenDate' => $forbiddenDate
        ));
    }

    private function createFolder($folder, $mode = 0775)
    {
        $createFolder = true;
        if(!is_dir($folder))
        {
            $oldmask = umask(0);
            $createFolder = mkdir($folder, $mode, true);
            umask($oldmask);
        }
        return $createFolder;
    }

    /**
     * @Route("/edit/{motwId}", name="atm_motw_edit")
     */
    public function editAction($motwId){
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request_stack')->getCurrentRequest();
        $config = $this->getParameter('atm_motw_config');

        $motw = $em->getRepository('ATMMotwBundle:Motw')->findOneById($motwId);
        $form = $this->createForm(MotwType::class,$motw,array(
            'user' => $config['user']
        ));

        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/';
                $this->createFolder($mediaDir);

                $motwFolder = $mediaDir.$motw->getFolderName();

                $thumbnailFileTour = $request->files->get('motwThumbnailTour');
                if($thumbnailFileTour){
                    if(!is_null($motw->getThumbnailTour()) && file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTour())){
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTour());
                    }

                    $extensionTour = $thumbnailFileTour->guessExtension();
                    $thumbNameTour = md5(uniqid()).'.'.$extensionTour;
                    $thumbnailFileTour->move($motwFolder,$thumbNameTour);
                    $motw->setThumbnailTour($config['media_folder'].'/'.$motw->getFolderName().'/'.$thumbNameTour);
                }

                $thumbnailFileTourMobile = $request->files->get('motwThumbnailTourMobile');
                if($thumbnailFileTourMobile){
                    if(!is_null($motw->getThumbnailTourMobile()) && file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTourMobile())){
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTourMobile());
                    }
                    $thumbNameTourMobile = md5(uniqid()).'.'.$thumbnailFileTourMobile->guessExtension();
                    $thumbnailFileTourMobile->move($motwFolder,$thumbNameTourMobile);
                    $motw->setThumbnailTourMobile($config['media_folder'].'/'.$motw->getFolderName().'/'.$thumbNameTourMobile);
                }


                $thumbnailFileMembers = $request->files->get('motwThumbnailMembers');
                if($thumbnailFileMembers){
                    if(file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembers())){
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembers());
                    }

                    $thumbNameMembers = md5(uniqid()).'.'.$thumbnailFileMembers->guessExtension();
                    $thumbnailFileMembers->move($motwFolder,$thumbNameMembers);
                    $motw->setThumbnailMembers($config['media_folder'].'/'.$motw->getFolderName().'/'.$thumbNameMembers);
                }

                $thumbnailFileMembersMobile = $request->files->get('motwThumbnailMembersMobile');
                if($thumbnailFileMembersMobile){
                    if(!is_null($motw->getThumbnailMembersMobile()) && file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembersMobile())){
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembersMobile());
                    }
                    $thumbNameMembersMobile = md5(uniqid()).'.'.$thumbnailFileMembersMobile->guessExtension();
                    $thumbnailFileMembersMobile->move($motwFolder,$thumbNameMembersMobile);
                    $motw->setThumbnailMembersMobile($config['media_folder'].'/'.$motw->getFolderName().'/'.$thumbNameMembersMobile);
                }

                $em->persist($motw);
                $em->flush();

                return $this->redirect($this->get('router')->generate('atm_motw_index'));
            }
        }

        $forbiddenDate = $em->getRepository('ATMMotwBundle:Motw')->getForbiddenDates();
        $forbiddenDate = !is_null($forbiddenDate) ? $forbiddenDate->modify('+1 day') : null;

        return $this->render('ATMMotwBundle:Admin:edit.html.twig',array(
            'form' => $form->createView(),
            'forbiddenDate' => $forbiddenDate,
            'motw' => $motw
        ));
    }

    /**
     * @Route("/delete/{motwId}", name="atm_motw_delete")
     */
    public function deleteAction($motwId){
        $em = $this->getDoctrine()->getManager();
        $motw = $em->getRepository('ATMMotwBundle:Motw')->findOneById($motwId);

        if(file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTour())){
            unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailTour());
        }

        if(file_exists($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembers())){
            unlink($this->get('kernel')->getRootDir().'/../web/'.$motw->getThumbnailMembers());
        }

        $em->remove($motw);
        $em->flush();

        return $this->redirect($this->get('router')->generate('atm_motw_index'));
    }
}