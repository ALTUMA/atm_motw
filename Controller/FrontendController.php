<?php

namespace ATM\MotwBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FrontendController extends Controller{

    /**
     * @Route("/details/{motwId}", name="atm_motw_details")
     */
    public function detailsAction($motwId){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('m')
            ->addSelect('mo')
            ->addSelect('mo_info')
            ->from('ATMMotwBundle:Motw','m')
            ->join('m.model','mo')
            ->join('mo.info','mo_info')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('m.id',$motwId),
                    $qb->expr()->lte('m.init_date','CURRENT_DATE()')
                )
            )
        ;

        $motw = $qb->getQuery()->getArrayResult();

        if(empty($motw)){
            $config = $this->getParameter('atm_motw_config');
            return $this->redirect($this->get('router')->generate($config['redirect_route_motw_not_exists']));
        }

        return $this->render('ATMMotwBundle:Frontend:details.html.twig',array(
            'motw' => $motw[0]
        ));
    }
}